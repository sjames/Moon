find_package(Git REQUIRED QUIET)

# VERSION_MAJOR/VERSION_MINOR/VERSION_PATCH/VERSION_METADATA Passed into this
# script from the main CMakeLists.txt
string(TIMESTAMP VERSION_BUILD_TIMESTAMP "%Y-%m-%d %H:%M:%S")
set(VERSION_STRING ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}${VERSION_METADATA})

set(GIT_SHA1 "")
set(GIT_SHA1_SHORT "")
set(GIT_BRANCH "")
set(GIT_TAG "")
set(GIT_DIRTY "")
set(GIT_COMMIT_AUTHOR "")
set(GIT_COMMIT_DATE "")
set(GIT_VERSION "")
set(GIT_EXTENDED_INFO "")
set(GIT_REPOSITORY_STATUS "")

# If there is a git repository, then we build and embedded some version information with it,
# we do this with a custom target that is always run, so the git header always has the most
# up to date information
if(EXISTS ${PROJECT_ROOT}/.git)
    if(GIT_FOUND)

        # Get the SHA1
        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_SHA1
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT EQUAL 0)
            message(WARNING "Failed to get the long SHA1 hash for the repository. Error: ${PROCESS_ERROR}")
        endif(NOT PROCESS_RESULT EQUAL 0)

        # Get the short version of the SHA1
        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse --short ${GIT_SHA1}
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_SHA1_SHORT
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT EQUAL 0)
            message(WARNING "Failed to get the short SHA1 hash for the repository. Error: ${PROCESS_ERROR}")
        endif(NOT PROCESS_RESULT EQUAL 0)

        # Get the branch it was built from
        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_BRANCH
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT EQUAL 0)
            message(WARNING "Failed to get the current branch for the repository. Error: ${PROCESS_ERROR}")
        endif(NOT PROCESS_RESULT EQUAL 0)

        # Detached head, see if this is a tag in detached mode
        if(GIT_BRANCH STREQUAL "HEAD")
            execute_process(
                COMMAND ${GIT_EXECUTABLE} describe --exact-match --tags HEAD
                WORKING_DIRECTORY ${SOURCE_DIR}
                OUTPUT_VARIABLE GIT_BRANCH
                RESULT_VARIABLE PROCESS_RESULT
                ERROR_VARIABLE PROCESS_ERROR
                OUTPUT_STRIP_TRAILING_WHITESPACE)

            # No tag, we are on a commit somewhere on a detached head, the version
            # tag will be built to reflect this
            if(NOT PROCESS_RESULT EQUAL 0)
                set(GIT_BRANCH "DETACHED")
            endif(NOT PROCESS_RESULT EQUAL 0)
        endif()

        # Get the tag
        execute_process(
            COMMAND ${GIT_EXECUTABLE} describe --tags --always HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_TAG
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        # Check for dirty source
        execute_process(
            COMMAND ${GIT_EXECUTABLE} diff-index --name-only HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_DIRTY
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT AND NOT GIT_DIRTY)
            set(SOURCE_DIRTY "")
        else()
            set(SOURCE_DIRTY "-dirty")
        endif(NOT PROCESS_RESULT AND NOT GIT_DIRTY)

        # Get the commit date
        execute_process(
            COMMAND ${GIT_EXECUTABLE} log -1 --pretty=format:%ad HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_COMMIT_DATE
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT EQUAL 0)
            message(WARNING "Failed to get the commit date for the last commit. Error: ${PROCESS_ERROR}")
        endif(NOT PROCESS_RESULT EQUAL 0)    

        # Get the commit author
        execute_process(
            COMMAND ${GIT_EXECUTABLE} log -1 --pretty=format:%cn HEAD
            WORKING_DIRECTORY ${SOURCE_DIR}
            OUTPUT_VARIABLE GIT_COMMIT_AUTHOR
            RESULT_VARIABLE PROCESS_RESULT
            ERROR_VARIABLE PROCESS_ERROR
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT PROCESS_RESULT EQUAL 0)
            message(WARNING "Failed to get the commit author for the last commit. Error: ${PROCESS_ERROR}")
        endif(NOT PROCESS_RESULT EQUAL 0)    

        # Build the version string
        # 1. DETACHED HEAD: DETACHED-SHA1
        # 2. On branch: TAG-SHA1
        if(GIT_BRANCH STREQUAL "DETACHED")
            set(GIT_VERSION "${GIT_BRANCH}-${GIT_SHA1_SHORT}${SOURCE_DIRTY}")
        else()
            set(GIT_VERSION "${GIT_TAG}-${GIT_SHA1_SHORT}${SOURCE_DIRTY}")
        endif(GIT_BRANCH STREQUAL "DETACHED")

        # Build some extended information about the commit
        set(GIT_EXTENDED_INFO "${GIT_COMMIT_AUTHOR}|${GIT_COMMIT_DATE}")

        # Build the final full status message for injection into the header. All
        # other variables are injected individually as a convenience 
        set(GIT_REPOSITORY_STATUS "${GIT_VERSION}|${GIT_EXTENDED_INFO}")
    else()
        message(WARNING "Git not found. Build will not contain git revision info.")
    endif()
endif(EXISTS ${PROJECT_ROOT}/.git)

configure_file(    
    "${CMAKE_CURRENT_LIST_DIR}/VersionConfig.h.in"
    "${CMAKE_CURRENT_BINARY_DIR}/VersionConfig.h.txt")

# copy the file to the final header only if the version changes
# reduces needless rebuilds
execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy_if_different VersionConfig.h.txt ${CMAKE_CURRENT_BINARY_DIR}/VersionConfig.h)
